var thrift = require('thrift');

var MultiplicationService = require('./gen-nodejs/MultiplicationService.js'), ttypes = require('./gen-nodejs/tutorial_types.js');

var tutorial = {};

var server = thrift.createServer(MultiplicationService, {
	multiply: function(n1, n2, result) {
		r = n1*n2;
		console.log("Server Multiplied: ", r);
		result(null, r);
	},
});

server.listen(9090);
