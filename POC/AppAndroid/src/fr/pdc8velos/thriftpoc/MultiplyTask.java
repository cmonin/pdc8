package fr.pdc8velos.thriftpoc;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.THttpClient;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;

import android.os.AsyncTask;
import android.util.Log;

public class MultiplyTask extends AsyncTask<Integer, Void, Integer> {
	
	private MainActivity mActivity;
	private Integer[] mOps;
	
	public MultiplyTask(MainActivity act) {
		mActivity = act;
	}
	
	@Override
	protected Integer doInBackground(Integer... params) {
		mOps = params;
		Integer product = null;
		
		TTransport transport = new TSocket("192.168.12.1", 9090);
		try {
			transport.open();
			
			TProtocol protocol = new  TBinaryProtocol(transport);
			MultiplicationService.Client client = new MultiplicationService.Client(protocol);
			
			if (params.length >= 2) {
				product = client.multiply(params[0], params[1]);
			} else {
				Log.w("Thrift", "one or two product params are missing");
			}
		} catch (TTransportException e) {
			e.printStackTrace();
		} catch (TException e) {
			e.printStackTrace();
		} finally {
			if (transport.isOpen()) {
				transport.close();
			}
		}
		
		return product;
	}
	
	@Override
	protected void onPostExecute(Integer result) {
		super.onPostExecute(result);
		if (result != null) {
			mActivity.updateProduct(mOps[0], mOps[1], result);
		} else {
			mActivity.updateProductError("Thrift error");
		}
	}
	
}
