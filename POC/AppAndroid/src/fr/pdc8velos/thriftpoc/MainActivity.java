package fr.pdc8velos.thriftpoc;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.TextView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		MultiplyTask task = new MultiplyTask(this);
		task.execute(3, 5);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void updateProduct(Integer op1, Integer op2, Integer result) {
		TextView tv = (TextView) findViewById(R.id.product);
		tv.setText(getResources().getString(R.string.product, op1, op2, result));
	}
	
	public void updateProductError(String error) {
		TextView tv = (TextView) findViewById(R.id.product);
		tv.setText(error);
	}

}
